import React from 'react';
import PropTypes from 'prop-types';
import '../assets/sass/main.scss';
import {BrowserRouter as Router, Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";

import LoginContainer from "../pages/login/Container";
import TodosContainer from "../pages/todos/Container";
import Header from "./Header";
import {getLoggedUser} from "../ducks/user";

const App = ({logged}) => (
    <React.Fragment>
        <Header/>
        <Router basename={process.env.REACT_APP_BASE_PATH}>
            <React.Fragment>
                <Route path="/" exact render={() => logged ? <div className="l-main"><TodosContainer/></div> : <Redirect to={{pathname: '/login'}}/>} />
                <Route path="/login" render={() => !logged ? <div className="l-login"><LoginContainer/></div> : <Redirect to={{pathname: '/'}}/>}/>
            </React.Fragment>
        </Router>
    </React.Fragment>
);

App.propTypes = {
    logged: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
    logged: !!getLoggedUser(state),
});

export default connect(mapStateToProps)(App);
