import {List, Record} from "immutable";

export function createModel(name, fromJson) {
    const Class = Record(fromJson({}), name);
    Class.fromServer = (json = {}) => new Class(fromJson(json));
    Class.fromServerList = (json = []) => List(json.map(Class.fromServer));

    return Class;
}

export function generateId() {
    let id = parseInt(localStorage.getItem('APP_SEQUENCE') || 0) + 1;
    localStorage.setItem('APP_SEQUENCE', id);

    return id;
}

export function identity(x) {
    return x;
}