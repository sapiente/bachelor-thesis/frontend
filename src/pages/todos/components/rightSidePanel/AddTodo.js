import React, {Component} from 'react';

import TodoForm from "./TodoForm";
import {bindActionCreators} from "redux";
import {addTodo} from "../../../../ducks/todo";
import {connect} from "react-redux";
import PropTypes from "prop-types";


class AddTodo extends Component {

    constructor(props) {
        super(props);

        this.state = {inEditMode: false};
        this.addTodo = this.addTodo.bind(this);
    }

    addTodo(name, priority, projectId, dueDate) {
        this.props.addTodo(name, priority, projectId, dueDate);
        this.setState({inEditMode: false});
    }

    render() {
        return (
            this.state.inEditMode ? (
                <TodoForm onSave={this.addTodo} onCancel={() => this.setState({inEditMode: false})}/>
            ) : (
                <div className='todo link' onClick={() =>  this.setState({inEditMode: true})}>
                    <i className="action fas fa-plus"/>
                    <span className="secondary-text">Přidat todo</span>
                </div>
            )
        );
    }
}

AddTodo.protoTypes = {
    addTodo: PropTypes.func.isRequired
};

const mapDispatchToProps  = (dispatch) => bindActionCreators({addTodo}, dispatch);

export default connect(null, mapDispatchToProps)(AddTodo);