import React, {Component} from 'react';
import {ReactDatez} from "react-datez";
import {bindActionCreators} from "redux";
import {addTodo} from "../../../../ducks/todo";
import {getProjects, getSelectedProjectId} from "../../../../ducks/project";
import connect from "react-redux/es/connect/connect";
import PropTypes from "prop-types";
import Todo from "../../../../models/Todo";
import * as ImmutablePropTypes from "react-immutable-proptypes";
import Project from "../leftSidePanel/Project";

class TodoForm extends Component {

    constructor(props) {
        super(props);

        const todo = this.props.todo || {projectId: this.props.selectedProjectId};

        this.state = {name: todo.name || '', dueDate: todo.dueDate || '', priority: todo.priority || 'low', projectId: todo.projectId || 0};
        this.save = this.save.bind(this);
    }

    save() {
        this.props.onSave(this.state.name, this.state.priority, this.state.projectId, this.state.dueDate)
    }

    render() {
        const {projects} = this.props;

        return (
            <React.Fragment>
                <div className="todo todo-add">
                    <div>
                        <select value={this.state.priority} onChange={(e) => this.setState({priority: e.target.value})}>
                            <option value="low">Nízká priorita</option>
                            <option value="normal">Normální priorita</option>
                            <option value="high">Vysoká priorita</option>
                        </select>
                    </div>
                    <div>
                        <input value={this.state.name} onChange={(e) => this.setState({name: e.target.value})} />
                    </div>
                    <div>
                        <select value={this.state.projectId} onChange={(e) => this.setState({projectId: parseInt(e.target.value)})}>
                            <option value={0}>Inbox</option>
                            {projects.map(project => <option key={project.id} value={project.id}>{project.name}</option>)}
                        </select>
                    </div>

                    <ReactDatez
                        handleChange={(dueDate) => this.setState({dueDate})}
                        value={this.state.dueDate}
                    />

                </div>
                <div className="todo-add-buttons" >
                    <div style={{margin: 0}} className="btn btn-primary" onClick={this.save}>Uložit</div>
                    <div className="btn btn-secondary" onClick={this.props.onCancel}>Zrušit</div>
                </div>
            </React.Fragment>
        );
    }
}

TodoForm.protoTypes = {
    todo : PropTypes.instanceOf(Todo),
    projects: ImmutablePropTypes.listOf(PropTypes.instanceOf(Project)).isRequired,
    selectedProjectId: PropTypes.number,
    onSave : PropTypes.func.isRequired,
    onCancel : PropTypes.func.isRequired,
};

const mapDispatchToProps  = (dispatch) => bindActionCreators({addTodo}, dispatch);

const mapStateToProps = (state) => ({
    projects: getProjects(state),
    selectedProjectId: getSelectedProjectId(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm);