import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addProject} from "../../../../ducks/project";
import ProjectForm from "./ProjectForm";

class AddProject extends Component {
    constructor(props) {
        super(props);
        this.state = {inEditMode: false};

        this.addProject = this.addProject.bind(this);
    }

    addProject(name) {
        this.props.addProject(name);
        this.setState({inEditMode: false})
    }

    render() {
        return (
            <div className='project' >
                {this.state.inEditMode ? (
                    <ProjectForm onSave={this.addProject} onCancel={() => this.setState({inEditMode: false})}/>
                ) : (
                    <div className='link' onClick={() =>  this.setState({inEditMode: true})}>
                        <i className="action fas fa-plus"/>
                        <span className="secondary-text">Přidat projekt</span>
                    </div>
                )}
            </div>
        );
    }
}

AddProject.protoTypes = {
    addProject: PropTypes.func.isRequired
};


const mapDispatchToProps = (dispatch) => bindActionCreators({
    addProject
}, dispatch);

export default connect(null, mapDispatchToProps)(AddProject);