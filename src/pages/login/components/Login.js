import React, {Component} from 'react';
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: null,
            password: null,
        };
    }

    isFormValid() {
        return !!(this.state.username && this.state.password);
    }

    render() {
        return (
            <div className="login">
                {this.props.errors.map((value, index) => <div className="error" key={index}>{value}</div>)}

                <input type="text" placeholder="Uživatelské jméno" name="username" onChange={(e) => {this.setState({username: e.target.value})}}/>
                <input type="password" placeholder="Heslo" name="password" onChange={(e) => {this.setState({password: e.target.value})}}/>
                <button className="btn btn-primary" disabled={!this.isFormValid()} onClick={() => this.props.attemptLogin(this.state.username, this.state.password)}>Login</button>
            </div>
        );
    }
}

Login.propTypes = {
    attemptLogin: PropTypes.func.isRequired,
    errors: ImmutablePropTypes.list.isRequired
};

export default Login;