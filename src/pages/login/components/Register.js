import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from "react-immutable-proptypes";

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {username: '', password: ''};
        this.register = this.register.bind(this);
    }

    isFormValid() {
        return !!(this.state.username && this.state.password);
    }

    register() {
        this.props.register(this.state.username, this.state.password, () => this.setState({username: '', password: ''}));
    }

    render() {
        return (
            <div className='login'>
                {this.props.errors.map((value, index) => <div key={index} className="error">{value}</div>)}

                <input type="text" name="username" value={this.state.username} onChange={(e) => this.setState({username: e.target.value})}/>
                <input type="password" name="password" value={this.state.password} onChange={(e) => this.setState({password: e.target.value})}/>
                <button className='btn btn-primary' disabled={!this.isFormValid()} onClick={this.register}>Registrovat</button>
            </div>
        );
    }
}

Register.propTypes = {
    register: PropTypes.func.isRequired,
    errors: ImmutablePropTypes.list.isRequired
};

export default Register;