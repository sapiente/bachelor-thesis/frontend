// Actions
import {List, Map} from 'immutable';
import {createSelector} from 'reselect'
import {identity} from "../utils/functions";
import {APP_ACTION_PREFIX} from "../utils/constants";
import Todo from "../models/Todo";
import moment from "moment";
import {DELETE_PROJECT, getSelectedProjectId} from "./project";
import {doDelete, doGet, doPatch, doPost} from "../core/fetch";

const ACTION_PREFIX = `${APP_ACTION_PREFIX}/todos`;

const FETCH_ALL = `${ACTION_PREFIX}/FETCH_ALL`;
const ADD_TODO = `${ACTION_PREFIX}/ADD_TODO`;
const EDIT_TODO = `${ACTION_PREFIX}/EDIT_TODO`;
const DELETE_TODO = `${ACTION_PREFIX}/DELETE_TODO`;
const SCHEDULE_TODO = `${ACTION_PREFIX}/SCHEDULE_TODO`;
const SET_FILTER = `${ACTION_PREFIX}/SET_FILTER`;

// Reducer
const defaultState = Map({
    filter: identity,
    isCalendar: false,
    all: List()
});

export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case FETCH_ALL: return state.set('all', action.payload);
        case ADD_TODO: return state.update('all', (all) => all.push(action.payload));
        case EDIT_TODO: return state.setIn(['all', findTodoIndexById(state.get('all'), action.payload.id)], action.payload);
        case DELETE_TODO: return state.removeIn(['all', findTodoIndexById(state.get('all'), action.payload)]);
        case SET_FILTER: return state.merge(action.payload);
        case SCHEDULE_TODO: return state.setIn(['all', findTodoIndexById(state.get('all'), action.payload.id), 'dueDate'], action.payload.dueDate);
        case DELETE_PROJECT: return state.update('all', (all) => all.filter((todo) => todo.projectId !== action.payload));
        default: return state;
    }
}

function findTodoIndexById(todos, id) {
    return todos.findIndex(todo => todo.id === id);
}

// Action Creators
export function fetchAll() {
    return (dispatch) => doGet("/todos")
        .then(({data}) => dispatch({ type: FETCH_ALL, payload: Todo.fromServerList(data)}));
}

export function addTodo(name, priority, projectId, dueDate) {
    return (dispatch) => doPost("/todos", {name, priority, projectId, dueDate})
        .then(({data}) => dispatch({ type: ADD_TODO, payload: Todo.fromServer(data)}));
}

export function editTodo(id, name, priority, projectId, done, dueDate) {
    return (dispatch) => doPatch(`/todos/${id}`, {name, projectId, priority, done, dueDate})
        .then(({data}) => dispatch({ type: EDIT_TODO, payload: Todo.fromServer(data)}));
}

export function deleteTodo(id) {
    return (dispatch) => doDelete(`/todos/${id}`)
        .then(() => dispatch({ type: DELETE_TODO, payload: id }));
}

export function setSearchFilter(search) {
    return { type: SET_FILTER, payload: {filter: (todo) => todo.name.includes(search), isCalendar: false } };
}


export function setCalendarFilter(selectedDate, view) {
    return { type: SET_FILTER, payload: {filter: (todo) => todo.dueDate && moment(todo.dueDate).isSame(selectedDate, view), isCalendar: true } };
}

export function resetFilter() {
    return { type: SET_FILTER, payload: {filter: identity, isCalendar: false} };
}


// Selectors
const getVisibilityFilter = (state) => state.todo.get('filter');
export const isCalendarFilter = (state) => state.todo.get('isCalendar');
export const getTodos = (state) => state.todo.get('all');

export const getVisibleTodos = createSelector([getSelectedProjectId, getTodos, getVisibilityFilter], (selectedProjectId, todos, filter) => {
    if(selectedProjectId !== null) {
        todos = todos.filter((todo) => todo.projectId === selectedProjectId);
    }

    return todos.filter(filter || identity).filter(filter || identity());
});