// Actions
import {APP_ACTION_PREFIX} from "../utils/constants";
import {Map} from 'immutable';
import {doPost, removeAuthToken, setAuthToken} from "../core/fetch";
import {destroyToken, storeToken} from "../core/auth";
import jwt_decode from 'jwt-decode'
import User from "../models/User";


const ACTION_PREFIX = `${APP_ACTION_PREFIX}/users`;

const FETCH_IDENTITY = `${ACTION_PREFIX}/FETCH_IDENTITY`;
const LOGOUT = `${ACTION_PREFIX}/LOGOUT`;

// Reducer
const defaultState = Map({
    loggedUser: null,
});

export default function reducer(state = defaultState, action = {}) {
    switch (action.type) {
        case FETCH_IDENTITY: return state.set('loggedUser', action.payload);
        case LOGOUT: return state.set('loggedUser', null);
        default: return state;
    }
}

// Action Creators
export function login(username, password) {
    return (dispatch) => doPost('/auth/login', {username, password})
            .then(({data}) => {
                storeToken(data.token);
                setAuthToken(data.token);
                dispatch(fetchIdentity(data.token))
            });
}

export function fetchIdentity(token) {
    return (dispatch) => new Promise((resolve) => resolve(jwt_decode(token)))
        .then((data) => dispatch({type: FETCH_IDENTITY, payload: User.fromServer(data)}))
}

export function register(username, password) {
    return () => doPost(`/users`, {username, password});
}

export function logout() {
    return (dispatch) => {
        destroyToken();
        removeAuthToken(null);
        dispatch({type: LOGOUT})
    }
}

// Selectors
export const getLoggedUser = (state) => state.user.get('loggedUser');