import {createModel} from "../utils/functions";
import moment from "moment";

const Todo = createModel("Todo", (json) => ({
    id: json.id,
    projectId: json.projectId || 0,
    userId: json.userId,
    name: json.name,
    isDone: json.done,
    dueDate: json.dueDate && moment(json.dueDate).format(),
    priority: json.priority,
}));

export default Todo;